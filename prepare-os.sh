#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

pacman-key --init
pacman-key --populate archlinuxarm

pacman -S --noconfirm python
