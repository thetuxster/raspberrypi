#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

bit_32=http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz
bit_64=http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-3-latest.tar.gz
device=/dev/mmcblk0

curl -sL -o archlinuxarm.tar.gz ${bit_32}

mkfs.vfat ${device}p1
mkfs.ext4 ${device}p2

[[ -d boot ]] && rmdir boot
[[ -d root ]] && rmdir root

mkdir boot root
mount ${device}p1 boot
mount ${device}p2 root

bsdtar -xpf archlinuxarm.tar.gz -C root
sync

mv root/boot/* boot/

install -d -g 1000 -o 1000 -m 0700  root/home/alarm/.ssh
install -g 1000 -o 1000 -m 0600 ~joseph/.ssh/authorized_keys root/home/alarm/.ssh/
install -m 0755 prepare-os.sh root/usr/local/bin/

echo alarm > root/etc/hostname

umount boot root
rmdir boot root
rm archlinuxarm.tar.gz
